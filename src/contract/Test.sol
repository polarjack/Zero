pragma solidity ^0.4.24;


contract Test {
    string public message;

    event Create(address user, string message);
    event FallBackYo(address who, string message);

    constructor(string _message) {
        message = _message;
        emit Create(msg.sender, message);
    }

    function() external payable {
        emit FallBackYo(msg.sender, message);
    }
}