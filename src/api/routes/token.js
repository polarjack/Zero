'use strict';

const express = require('express');
const keythereum = require('keythereum');
const userKeyfile = require('../../keyfile/mykey.json');
const router = express.Router();

var {
  web3,
  eth,
  getNCCUTokenDeployData,
  NCCUToken,
  NCCUTokenAbi,
  NCCUTokenBin,
} = require('../util/web3Setup');
const { admin } = require('../util/admin.js');
const { signTX } = require('../util/signTx.js');

const sample = {
  chainId: 42,
  data:
    '0xa9059cbb0000000000000000000000003e7af8b8c19c404670c1470273bca449148df4ed00000000000000000000000000000000000000000000003635c9adc5dea00000',
  gas: '0xae8a',
  gasPrice: '0x3b9aca00',
  nonce: '0x57',
  to: '0x3830f7Af866FAe79E4f6B277Be17593Bf96beE3b',
  value: '0x0',
};

router.get('/', (req, res) => {
  res.send('this is fucking nccu token api');
});

router.get('/create', async (req, res) => {
  // var keyInfo = await web3.eth.accounts.wallet.decrypt([admin], 'techfin');
  let data = await getNCCUTokenDeployData('NCCU Token', 'NCCU', 18, 5566556);
  // keyInfo = keyInfo['0'];

  // console.log(remixInputData.localeCompare(data));

  let privateKey = await keythereum.recover('techfin', userKeyfile);

  let nonce = await web3.eth.getTransactionCount('0x' + userKeyfile.address);

  let transaction = {
    data,
    nonce,
    gas: 4000000,
    gasPrice: 500000000000,
    value: '0x0',
  };

  let rawTx = signTX(privateKey, transaction);

  web3.eth.sendSignedTransaction(rawTx).on('receipt', console.log);

  res.json({
    rawTx,
  });
});

let sampleTX =
  '0x49ad752a0f65b3650fb7361a1050c2335e9cec6040cf3b400441e07d6750decf';
let sampleContract = '0xdf7518fE1aE19E8e4A74F1d17d99c852814eA30b';

router.post('/transfer', (req, res) => {
  let toSign = {};

  res.json({
    data: toSign,
    gas: 2000000,
    gasPrice: '20000000000',
  });
});

router.get('/testing', async (req, res) => {
  const counting = await eth.getTransactionCount('0x' + userKeyfile.address);
  res.json({
    result: counting,
  });
});

router.get('/balanceOf', async (req, res) => {
  const myContract = new web3.eth.Contract(
    NCCUTokenAbi,
    '0x3C6aE9250C67e3f3fc9a15Ff9E1E35eCee1Ec351',
  );

  let balance = await myContract.methods.balanceOf(userKeyfile.address).call();

  res.send({
    balance,
  });
});

router.post('/approved', (req, res) => {});

router.post('/transferFrom', (req, res) => {});
router.post('/allowance', (req, res) => {});

module.exports = router;
