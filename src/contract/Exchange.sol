pragma solidity ^0.4.25;

import "./interface/ERC20.sol";

contract Exchange {
  address public admin;

  address public owner;
  address public myToken;
  uint256 public myAmount;
  address public targetToken;
  uint256 public targetAmount;

  event ExchangeCreate(address creator,address creatorToken, uint256 creatorAmount, address targetToken, uint256 targetAmount);
  event ExchangeDone(address receiver);

  constructor() {
    admin = msg.sender;
  }
  function exchangeCreate(address _myToken, uint256 _myAmount,address _targetToken, uint256 _targetAmount) {
     // 注意要先 在 原本的 token 進行 approve

    require(ERC20(_myToken).transferFrom(msg.sender, this, _myAmount));
    
    owner = msg.sender;
    myToken = _myToken;
    myAmount = _myAmount;
    targetToken = _targetToken;
    targetAmount = _targetAmount;
    emit ExchangeCreate(msg.sender, _myToken, _myAmount, _targetToken, _targetAmount);
  }

  function acquire() {
    // 注意要先 在 原本的 token 進行 approve
    require(ERC20(targetToken).transferFrom(msg.sender, this, targetAmount));

    ERC20(myToken).transfer(msg.sender, myAmount);
    ERC20(targetToken).transfer(owner, targetAmount);
    emit ExchangeDone(msg.sender)
  }
}